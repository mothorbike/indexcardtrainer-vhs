//@OnlyCurrentDoc

function onOpen() {
  SpreadsheetApp
    .getUi()
    .createAddonMenu()
    .addItem("Start ...", "menuStartTraining")
    .addSubMenu(sortBuildSubMenu())
    .addSubMenu(
      SpreadsheetApp.getUi().createMenu("Werkzeuge")
        .addItem("Installation ...", "menuInstallation")
        .addItem("Initialisiere neu angelegte Karten", "menuInitializeCreatedCards")
        .addItem("bedingte Formatierung für Spalte B", "menuFormatAktColumn")
        .addItem("Datumsformat für Spalte L", "menuFormatDateColumn")
        .addItem("Optimale Spaltenbreite", "menuAutoresizeColumns")
    )
    .addToUi();
}

function menuInstallation() {
  var widget = HtmlService.createHtmlOutputFromFile("Installation.html")
    .setHeight(config.dialog.height)
    .setWidth(config.dialog.width);
  SpreadsheetApp.getUi().showModalDialog(widget, "IndexCardTrainer VHS - Installation");
}

function menuStartTraining() {
  var widget = HtmlService.createHtmlOutputFromFile("Training.html")
    .setHeight(config.dialog.height)
    .setWidth(config.dialog.width);
  SpreadsheetApp.getUi().showModalDialog(widget, "IndexCardTrainer VHS");
}

function menuAutoresizeColumns() {
  const sheet = getSheetIndexCards();
  sheet.autoResizeColumns(1, sheet.getLastColumn());
}

function menuFormatAktColumn() {
  const sheet = getSheetIndexCards();
  const range = sheet.getRange("B2:B");
  range.clearFormat();
  rules = sheet.getConditionalFormatRules();
  rules.push(SpreadsheetApp.newConditionalFormatRule()
    .whenNumberEqualTo(1)
    .setBackground('#ffffff')
    .setRanges([range])
    .build()
  );
  rules.push(SpreadsheetApp.newConditionalFormatRule()
    .whenNumberNotEqualTo(1)
    .setBackground('#ff8888')
    .setRanges([range])
    .build()
  );
  sheet.setConditionalFormatRules(rules);
  SpreadsheetApp.getUi().alert(
    "Spalte B wird hellrot markiert, wenn sie vom IndexCardTrainer nicht berücksichtigt wird. Das ist dann der Fall, wenn der Inhalt der Zelle nicht 1 ist."
  );
}

function menuFormatDateColumn() {
  const range = getSheetIndexCards().getRange("L2:L");
  range.setNumberFormat("dd.MM.yyyy hh:mm:ss");
}

function menuInitializeCreatedCards() {
  const fullRange = getSheetIndexCards().getDataRange();
  const orgaRange = getSheetIndexCards().getRange(
    2, IndexCard.columns.total_count + 1, fullRange.getHeight() - 1, 3
  );
  const values = orgaRange.getValues();
  for (const index in values) {
     if ( !Number.isSafeInteger(values[index][0]) ) {
       values[index][0] = 0;
     }
     if ( !Number.isSafeInteger(values[index][1]) ) {
       values[index][1] = 0;
     }
     if ( values[index][2] == '' ) {
       values[index][2] = new Date('2000-01-01T12:00:00');
     }
  }
  orgaRange.setValues(values);
}

//
// Api
//

function apiBuildSheetIndexCardTrainer() {
  const listOfSheets = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  const listOfSheetNames = listOfSheets.map((s) => s.getName());
  if ( listOfSheetNames.indexOf(config.sheet_indexcards) != -1 ) {
    SpreadsheetApp.getUi().alert(`Es existiert bereits ein Blatt mit dem Namen "${config.sheet_indexcards}".`);
    return;
  }
  const sheet = SpreadsheetApp.getActiveSpreadsheet().insertSheet(config.sheet_indexcards);
  sheet.getRange("A1:L1")
    .setValues([[
      "Id", 
      "Akt", 
      "Quelle", 
      "Fremdsprache", 
      "Aussprache", 
      "Deutsch", 
      "Hinweis (öffentlich)", 
      "Hinweis (geheim)", 
      "Gruppen", 
      "Anzahl", 
      "Box", 
      "letzte Abfrage"
    ]])
    .setFontWeight("bold");
  sheet.setFrozenRows(1);
  sheet.getRange("A2:B")
    .setNumberFormat("0");
  menuFormatAktColumn();
  sheet.getRange("J2:K")
    .setNumberFormat("0");
  sheet.getRange("A2:I3")
    .setValues([
      ["1", "1", "Installation", "to be",  "tu bi", "sein", "Verb", "", "Verb"],
      ["2", "1", "Installation", "word", "wörd", "das Wort", "", "", "Verb"]
    ]);
  menuFormatDateColumn();
  menuInitializeCreatedCards();
}

function apiGetBoxStats() {
  const values = getDataRangeWithoutHeaders().getValues();
  const boxStats = Array(config.max_box).fill(0);
  for (let row of values) {
    boxStats[row[IndexCard.columns.box]]++;
  }
  return boxStats;
}

function apiGetLanguages() {
  const range = getRowRange(1);
  const values = range.getValues();
  return {
    'first':  values[0][IndexCard.columns.content1],
    'second': values[0][IndexCard.columns.content2],
  };
}

function apiGetMaxBox() {
  return config.max_box;
}

function apiGetNextIndexCard(conf) {
  const boxStats = apiGetBoxStats();
  const rowIndexSheet = getFilteredRowIndexSheetByStrategy(conf);
  if ( rowIndexSheet == -1 ) return noIndexCardResult(boxStats);
  const values = getRowRange(rowIndexSheet).getValues();
  const card = new IndexCard({ 
    rowIndexSheet, 
    data: values[0],
    languages: apiGetLanguages(),
    direction: conf.direction,
  });
  return { card, boxStats };
}

function apiRegisterView(rowIndexSheet) {
  setLastViewDate(rowIndexSheet);
  increaseAmount(rowIndexSheet);
  SpreadsheetApp.flush();
}

function apiRegisterCorrectAnswer(rowIndexSheet) {
  increaseBox(rowIndexSheet);
  SpreadsheetApp.flush();
}

function apiRegisterWrongAnswer(rowIndexSheet) {
  decreaseBox(rowIndexSheet);
  SpreadsheetApp.flush();
}

//
// Config
//

const config = (() => {
  return {
    timezone: 'Europe/Berlin',
    sheet_indexcards: 'Vokabeln',
    max_box: 5,
    dialog: {
      height: 400,
      width: 500,
    },
  };
})();

//
// Filter
//

function getFilteredRowIndexSheetByStrategy(conf) {
  const filter = new Filter(conf.filter);
  filter.getIndexList();
  if ( filter.indexList.length == 0 ) return -1;
  if ( conf.strategy == "oldest" ) {
    return filter.findOldest();
  }
  return filter.findRandom();
}

function getFilteredIndexList(filterConf) {
  const filter = new Filter(filterConf);
  return filter.getIndexList();
}

function Filter(filter) {
  this.values = getDataRangeWithoutHeaders().getValues();
  this.filter = filter;
  this.indexList = [];
}

Filter.prototype.getIndexList = function () {
  this.buildUnfilteredIndexList();
  this.filterActive();
  this.filterByBox();
  this.filterBySource();
  this.filterByGroup();
  return this.indexList;
}

Filter.prototype.buildUnfilteredIndexList = function () {
  this.indexList = this.values.map((value, index) => index);
}

Filter.prototype.findOldest = function () {
  if ( this.indexList.length == 0 ) return -1; 
  var oldestIndex = this.indexList[0];
  var oldestLastView = this.values[oldestIndex][IndexCard.columns.last_view];
  for ( let index of this.indexList ) {
    const lastView = this.values[index][IndexCard.columns.last_view];
    if ( lastView == '' ) return index + 2;
    if ( lastView < oldestLastView ) {
      oldestIndex = index;
      oldestLastView = lastView;
    }
  }
  return oldestIndex + 2;
}

Filter.prototype.findRandom = function () {
  if ( this.indexList.length == 0 ) return -1; 
  return this.indexList[getRandomInt(this.indexList.length)] + 2;
}

Filter.prototype.filterActive = function () {
  this.indexList = this.indexList.filter(
    (index) => this.values[index][IndexCard.columns.active] == 1
  );
}

Filter.prototype.filterByBox = function () {
  if ( "box" in this.filter || this.filter.box == "none" ) return ;
  this.indexList = this.indexList.filter(
    (index) => this.values[index][IndexCard.columns.box] == this.filter.box
  );
}

Filter.prototype.filterByGroup = function () {
  if ( "group" in this.filter || this.filter.group == "" ) return ;
  this.indexList = this.indexList.filter(
    (index) => this.values[index][IndexCard.columns.groups].indexOf(this.filter.group) != -1
  );
}

Filter.prototype.filterBySource = function () {
  if ( "source" in this.filter || this.filter.source == "" ) return ;
  this.indexList = this.indexList.filter(
    (index) => this.values[index][IndexCard.columns.source].indexOf(this.filter.source) != -1
  );
}

//
// IndexCard
//

function IndexCard(dataObj) {
  const cols = IndexCard.columns;
  const data = dataObj.data;
  const drctn = this.getDrctn(dataObj);

  this.rowIndexSheet = dataObj.rowIndexSheet;
  this.id = data[cols.id];
  this.active = data[cols.active];
  this.source = data[cols.source];
  this.language1 = dataObj.languages[drctn ? 'first' : 'second'];
  this.content1 = data[drctn ? cols.content1 : cols.content2];
  this.pronunciation1 = drctn ? data[cols.pronunciation] : '';
  this.language2 = dataObj.languages[drctn ? 'second' : 'first'];
  this.content2 = data[drctn ? cols.content2 : cols.content1];
  this.pronunciation2 = drctn ? '' : data[cols.pronunciation];
  this.hint_public = data[cols.hint_public];
  this.hint_secret = data[cols.hint_secret];
  this.groups = data[cols.groups];
  this.box = data[cols.box];
  this.total_count = data[cols.total_count];
  const last_view = data[cols.last_view];
  this.last_view = ( last_view instanceof Date ) 
    ? data[cols.last_view].toISOString()
    : '';
}

IndexCard.prototype.getDrctn = function (dataObj) {
  var direction = dataObj.direction;
  if ( this.isRandomDirection(direction) ) {
    direction = Math.random() < 0.5 ? IndexCard.directions[1] : IndexCard.directions[2];
  }
  return ( direction == IndexCard.directions[1] );
}

IndexCard.prototype.isRandomDirection = function (direction) {
  return direction == IndexCard.directions[0] 
    || !IndexCard.directions.includes(direction);
}

IndexCard.columns = {
  id: 0,
  active: 1,
  source: 2,
  content1: 3,
  pronunciation: 4,
  content2: 5,
  hint_public: 6,
  hint_secret: 7,
  groups: 8,
  total_count: 9,
  box: 10,
  last_view: 11,
};

IndexCard.columnCount = Object.keys(IndexCard.columns).length;

IndexCard.directions = ['random', 'first-second', 'second-first'];

//
// Log
//

function logObject(obj, name = 'Object') {
  for (const [key, value] of Object.entries(config)) {
    Logger.log(`${name}[${key}]: ${value}`);
  }
}

function logRange(range) {
  Logger.log(range.getA1Notation());
}

//
// Sorting
//

const sortConfig = (() => {
  return {
    ByPos: {
      label: "Pos ↑",
      sortSpec: [
        { column: IndexCard.columns.id + 1, ascending: true}, 
      ]
    },
    ByBoxAndLastView: {
      label: "Box ↑, letzte Abfrage ↑",
      sortSpec: [
        { column: IndexCard.columns.box + 1, ascending: true}, 
        { column: IndexCard.columns.last_view + 1, ascending: true}
      ]
    },
    ByLanguage1: {
      label: "Sprache 1 ↑",
      sortSpec: [
        { column: IndexCard.columns.content1 + 1, ascending: true}, 
      ]
    },
    ByLanguage2: {
      label: "Sprache 2 ↑",
      sortSpec: [
        { column: IndexCard.columns.content2 + 1, ascending: true}, 
      ]
    },
    ByLastView: {
      label: "letzte Abfrage ↑",
      sortSpec: [
        { column: IndexCard.columns.last_view + 1, ascending: true}
      ]
    },
  };
})();

function sortGetFuncName(sortKey) {
  return `sort${sortKey}`
}

for (const sortKey of Object.keys(sortConfig)) {
  let funcName = sortGetFuncName(sortKey);
  this[funcName] = function() {
    const range = getDataRangeWithoutHeaders();
    range.sort(sortConfig[sortKey].sortSpec);
  };
}

function sortBuildSubMenu() {
  const subMenu = SpreadsheetApp.getUi().createMenu("Sortieren");
  for (const sortKey of Object.keys(sortConfig)) {
    let funcName = sortGetFuncName(sortKey);
    subMenu.addItem(sortConfig[sortKey].label, funcName);
  }
  return subMenu;
}

//
// functions
//

function getSheetIndexCards() {
  return SpreadsheetApp.getActiveSpreadsheet().getSheetByName(config.sheet_indexcards);
}

function getDataRangeWithoutHeaders() {
  const fullRange = getSheetIndexCards().getDataRange();
  return getSheetIndexCards().getRange(2, 1, fullRange.getHeight() - 1, fullRange.getWidth());
}

function getRandomInt(max) {
  // return a value from range [0 ... max-1]
  return Math.floor(Math.random() * max);
}

function noIndexCardResult(boxStats) {
  return {
    card: { rowIndexSheet: 0, },
    boxStats
  }
}

function getRowRange(rowIndexSheet) {
  return getSheetIndexCards().getRange(rowIndexSheet, 1, 1, IndexCard.columnCount);
}

function increaseAmount(rowIndexSheet) {
  const range = getSheetIndexCards().getRange(rowIndexSheet, IndexCard.columns.total_count + 1);
  logRange(range);
  const amount = range.getValue();
  range.setValue([amount + 1]);
}

function increaseBox(rowIndexSheet) {
  const range = getSheetIndexCards().getRange(rowIndexSheet, IndexCard.columns.box + 1);
  logRange(range);
  const box = range.getValue();
  if ( box < config.max_box - 1 ) {
    range.setValue([box + 1]);
  }
}

function decreaseBox(rowIndexSheet) {
  const range = getSheetIndexCards().getRange(rowIndexSheet, IndexCard.columns.box + 1);
  logRange(range);
  const box = range.getValue();
  if ( box > 0 ) {
    range.setValue([box - 1]);
  }
}

function setLastViewDate(rowIndexSheet) {
  const range = getSheetIndexCards().getRange(rowIndexSheet, IndexCard.columns.last_view + 1);
  logRange(range);
  range.setValue([new Date()]);
}
