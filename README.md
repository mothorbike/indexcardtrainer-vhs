# IndexCardTrainer

## Inhalt

- [Beschreibung](#beschreibung)
- [Vorbereitungen](#vorbereitungen)
    - [Voraussetzungen](#voraussetzungen)
    - [WICHTIGER HINWEIS](#wichtiger-hinweis)
    - [Ersteinrichtung / "Installation"](#ersteinrichtung--installation)
- [Loslegen](#loslegen)
    - [Vokabeln lernen und abfragen](#vokabeln-lernen-und-abfragen)
    - [Pflege der Vokabeln](#pflege-der-vokabeln)
    - [Sortieren](#sortieren)
- [Tipps](#tipps)
    - [Klein anfangen](#klein-anfangen)
    - [Mit mehreren Tabellen arbeiten](#mit-mehreren-tabellen-arbeiten)
    - [Nicht nur für Vokabeln](#nicht-nur-für-vokabeln)
- [Ausblick](#ausblick)
    - [Ideen](#ideen)

## Beschreibung

Das Tool simuliert die Lernmethode nach Dateikarten und ist speziell auf der Lernen von Vokabeln ausgelegt. Dabei werden die Karteikarten in Boxen organisiert (siehe https://de.wikipedia.org/wiki/Lernkartei). 

Wie bei den Karteikarten auch, sind in der Tabelle alle Infos offen zugänglich. Wie bei Karteikarten kann man sich hier auch selbst "betrügen". 

Es gibt zu der Lernmethode eigene Webseiten, wie auch Programme, die man lokal installieren kann. Aber das hat alles nicht meinen Vorstellungen entsprochen. Zumal die Daten meist in Formaten vorgehalten werden, die keinen Austausch zulassen und ein Wechsel immer mit einer kompletten Neueingabe aller Daten verbunden ist. Man kann so etwas auch in Excel oder LibreOffice umsetzen. Ich habe mich hier für Google Tabellen entschieden, da diese einfach und kostenlos für jedermann zugänglich sind. 

## Vorbereitungen

### Voraussetzungen

- Eine Nutzung ist nur am Desktop möglich, nicht auf dem Smartphone
    - Das ist eine Einschränkung von Google, nicht von mir. 
- Google-Account für Zugriff auf Google Tabellen (http://sheets.google.com/)

### WICHTIGER HINWEIS

Google Apps Script ist nicht sonderlich schnell, aber auch nicht so langsam, dass es gar keinen Spaß macht. Allerdings war die Menge der Vokabeln bisher eher klein. 

### Ersteinrichtung / "Installation"

Unter https://gitlab.com/mothorbike/indexcardtrainer-vhs/-/wikis/Installation 
findest du eine ausführliche Anleitung zur Installation. 
Jeder Schritt wird erklärt, auch für Benutzer, die technisch nicht so versiert sind. 
Dafür bekommst du ein kostenloses Werkzeug, um deine Vokabeln zu lernen. 

## Loslegen

### Vokabeln lernen und abfragen

Über das Menü `Erweiterungen` > `IndexCardTrainer` > `Start ...` startest du die Anwendung. 

Zuerst öffnen sich die `Einstellungen`. Neben der Richtung kannst du eine Box, eine Quelle und/oder eine Gruppe bestimmen, die Strategie für die Auswahl der nächsten Karte wählen und entscheiden, ob und wie du die Aussprache ausgegeben haben willst. Danach wechselst du mit Klick auf den Button `Lernen` zum Kern der Anwendung. 

Ich habe das Lernen und Abfragen nicht getrennt umgesetzt, sondern in einem gemeinsamen Dialog. Beim Lernen mit Karteikarten gibt es ja auch keine getrennten Karten oder Boxen zum Lernen und Abfragen, sondern man selbst entscheidet, ob man die Karten auswendig lernen will oder wissen will, wie gut man die Karten schon auswendig kennt. 

Es gibt einen `Hilfe`-Button, in dem die grundlegende Funktionsweise des Dialogs erklärt ist. 

Wie man die Vokabeln abfragt, also alle oder nur einen Teil, bleibt einem selbst überlassen. 

### Pflege der Vokabeln

Die Vokabeln liegen in einer Google Tabelle. Dabei wird nur das Tabellenblatt mit dem Namen `Vokabeln` abgefragt. 

Die erste Zeile enthält die Spaltenbeschriftungen. Die Spalten sind fest einprogrammiert, dürfen also nicht getauscht werden. Eine Erläuterung zu den Spalten und deren Bedeutung/Verwendung findest du unter https://gitlab.com/mothorbike/indexcardtrainer-vhs/-/wikis/Aufbau-der-Vokabel-Tabelle

Die Tabelle enthält zwar einige Spalten, meist genügt es jedoch, die Spalten `Akt` (Spalte B), `Quelle` (Spalte C), `Sprache1` (Spalte D) und `Sprache2` (Spalte F) auszufüllen. 
In einigen Fällen kann ein `Hinweis (öffentlich)` (Spalte G) sinnvoll sein. 

Über das Menü `Erweiterungen` > `IndexCardTrainer` > `Werkzeuge` > `Initialisiere neue Einträge` können die Spalten `Anzahl` (Spalte J), `Box` (Spalte K) und `letzte Abfrage` (Spalte L) vorausgefüllt werden. 
Man gibt also nur wie im vorherigen Absatz beschrieben die für die neuen Vokabeln relevanten Daten ein und geht anschließend auf `Initialisiere neue Einträge`, um die Spalten `Anzahl`, `Box` und `letzte Abfrage` korrekt zu setzen. Es werden nur leere Zellen geändert oder wenn in den Spalten `Anzahl` oder `Box` keine Zahl steht. 

### Sortieren

Die Vokabeln können über das Menü `IndexCardTrainer` > `Sortieren` nach verschiedenen Kriterien sortiert werden. 

## Tipps

Alle Tipps sind nur Vorschläge. Jeder weiß selbst am Besten, was ihm oder ihr gut tut und wie er am Besten lernt und arbeitet. 

### Klein anfangen

Ich empfehle, erst Mal wenige Vokabeln anzulegen und diese erst Mal zu lernen, bis diese gut sitzen, bevor weitere Vokabeln angegangen werden. Eventuell kann man später der Lerntempo erhöhen. 

Mit zu vielen Vokabeln am Anfang besteht die Gefahr, dass man die Lust verliert und dann aufgibt. 

### Weitere Probleme bei Vokablen

Mit der Zeit stolpert man über immer wieder ähnliche Probleme, für die ich bisher keine befriedigende Lösung gefunden habe. Ein paar möchte ich hier mal kurz aufführen: 

- Übersetzungen sind oft nicht eindeutig und manchmal auch vom Kontext abhängig. Das fängt bei Präpositionen an und geht letztendlich über alle Wortgruppen. 
    - Beispiel: das Wort "sie" ist einmal das Personalpronomen in der 3. Person singular weiblich, aber auch in der 3. Person plural oder die Höflichkeitsform sein. 
    - Beispiel: das Wort "sein" ist einmal das Hilfsverb, aber auch das besitzanzeigende Fürwort ("sein Haus"). 
    - Lösung: ein entsprechender Hinweis im Feld `Hinweis (öffentlich)` (Spalte G)
- Oft kommt eine Vokabel in einem Lehrbuch an mehreren Stellen und wenn man Pech hat auch noch mit unterschiedlichen Übersetzungen vor. 
- Wie notiere ich deutsche Hauptwörter ? 
    - mit vorangestelltem Artikel: das Haus
    - mit nachgestelltem Artikel: Haus, das
    - mit Hinweis auf das Geschlecht: Haus (m)
    - ohne Artikel: Haus 
        - dann fehlt das Geschlecht. 

### Nicht nur für Vokabeln

Man kann mit dem IndexCardTrainer gut Vokabeln lernen. Man kann aber anstelle von Vokabeln auch anderen Lerninhalte einpflegen. Dazu zählen zum Beispiel Phrasen, Redewendungen oder ganze Sätze. Auch Grammatik, wie Konjugationen, Deklinationen und viele andere Themen lassen sich gut mit dem IndexCardTrainer umsetzen. 

Aktuell sind hier nur Textinhalte möglich, das schränkt die Nutzung des IndexCardTrainers aber nur bedingt ein. 

## Ausblick

Ich habe einige Ideen, allerdings ist es eine Frage der zur Verfügung stehenden Zeit und auch der Lust, wie ich hier weiter mache. Es kommen auch gerne mal andere Ideen von der Seite, die ich vorziehe. 

### ToDos und Ideen

- Ausgabe der Spalte `Gruppen` (Spalte I)
- Mehrsprachiges Benutzerinterface
- Umsetzung als WebApp und/oder AddOn
- neue Lernstrategien
 